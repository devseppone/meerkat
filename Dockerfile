FROM debian:testing
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install cmake gcc-8 python3-pip
RUN pip3 install conan ninja
COPY . /meerkat
RUN conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
RUN conan install /meerkat
RUN rm -rf /meerkat
