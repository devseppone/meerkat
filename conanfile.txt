[requires]
catch2/2.4.0@bincrafters/stable
libuv/1.23.2@bincrafters/stable
spdlog/1.2.1@bincrafters/stable

[generators]
cmake
