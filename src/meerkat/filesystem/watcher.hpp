#ifndef WATCHER_HPP
#define WATCHER_HPP

#include <meerkat/filesystem.hpp>

#include <cstddef>

namespace meerkat::filesystem {

using Files = std::vector<Path>;

class EventLoop;

class Watcher {
  friend class EventLoop;

 public:
  Watcher(Path path, FileEventHandler handler);

  ~Watcher() = default;

  auto files() const -> const Files& { return m_files; }
  auto handler() const -> const FileEventHandler& { return m_handler; }

 private:
  Files m_files;
  FileEventHandler m_handler;
};

}  // namespace meerkat::filesystem

#endif  // WATCHER_HPP
