#include <catch2/catch.hpp>
#include <filesystem>
#include <fstream>
#include <meerkat/filesystem.hpp>
#include <meerkat/filesystem/event_loop.hpp>

namespace fs = std::filesystem;

SCENARIO("Watching filesystem for events") {
  namespace mkfs = meerkat::filesystem;

  GIVEN("An existing file") {
    const mkfs::Path fileToWatch{fs::temp_directory_path() / "watchMe.txt"};
    std::ofstream fileStream{fileToWatch};
    REQUIRE(fs::is_regular_file(fileToWatch));

    WHEN("file is watched by meerkat") {
      bool meerkatRecognizedChange = false;
      mkfs::watch(fileToWatch, [&](fs::path const& file) {
        REQUIRE(fileToWatch == file);
        meerkatRecognizedChange = true;
      });

      AND_WHEN("file is modified") {
        fileStream << "file was modified";
        fileStream.close();

        mkfs::EventLoop::instance().process();

        THEN("meerkat should recognize that change") {
          REQUIRE(meerkatRecognizedChange);
        }
      }
    }
  }
}
