#include <meerkat/filesystem/watcher.hpp>

#include <filesystem>
#include <functional>

namespace meerkat::filesystem {

using Path = std::filesystem::path;
using FileEventHandler = std::function<void(Path const& path)>;

void watch(Path const&, FileEventHandler const&);

auto exec() -> int;

}  // namespace meerkat::filesystem
