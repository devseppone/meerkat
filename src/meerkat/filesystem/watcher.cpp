#include <meerkat/filesystem.hpp>
#include <meerkat/filesystem/watcher.hpp>

#include <uv.h>

namespace meerkat::filesystem {

Watcher::Watcher(Path path, FileEventHandler handler)
    : m_files{path}, m_handler(std::move(handler)) {}

}  // namespace meerkat::filesystem
