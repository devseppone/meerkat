#ifndef EVENT_LOOP_HPP
#define EVENT_LOOP_HPP

#include <meerkat/filesystem.hpp>

#include <unordered_map>

namespace meerkat::filesystem {

using EventHandlerTable = std::unordered_map<std::string, FileEventHandler>;

class Watcher;

class EventLoop {
 public:
  static EventLoop& instance() {
    static EventLoop instance;
    return instance;
  };

  EventLoop() = default;
  ~EventLoop() = default;

  EventLoop(EventLoop const&) = delete;
  void operator=(EventLoop const&) = delete;
  EventLoop(EventLoop&&) = delete;
  void operator=(EventLoop&&) = delete;

  auto add(Watcher const&) -> void;

  auto process() -> void;

  auto exec() -> int;

  auto apply(std::filesystem::path) -> void;

 private:
  EventHandlerTable m_handlerTable;
};

}  // namespace meerkat::filesystem

#endif  // EVENT_LOOP_HPP
