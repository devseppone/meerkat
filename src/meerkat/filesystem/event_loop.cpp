#include <meerkat/filesystem/event_loop.hpp>
#include <meerkat/filesystem/watcher.hpp>

#include <uv.h>
#include <chrono>
#include <thread>

namespace meerkat::filesystem {

static void applyEventHandler(uv_fs_event_t* handle, const char*, int, int) {
  EventLoop::instance().apply(Path{handle->path});
}

void EventLoop::add(Watcher const& watcher) {
  for (auto const& file : watcher.files()) {
    m_handlerTable[file.string()] = watcher.handler();
    auto fs_event_req =
        static_cast<uv_fs_event_t*>(malloc(sizeof(uv_fs_event_t)));
    uv_fs_event_init(uv_default_loop(), fs_event_req);
    std::string filePath = file.string();
    uv_fs_event_start(fs_event_req, applyEventHandler, filePath.c_str(),
                      UV_FS_EVENT_RECURSIVE);
  }
}

void EventLoop::process() {
  uv_run(uv_default_loop(), UV_RUN_ONCE);
}

int EventLoop::exec() {
  using namespace std::chrono_literals;
  uv_idle_t idler;

  uv_idle_init(uv_default_loop(), &idler);
  uv_idle_start(&idler, [](uv_idle_t*) { std::this_thread::sleep_for(100ms); });

  return uv_run(uv_default_loop(), UV_RUN_DEFAULT);
}

void EventLoop::apply(Path path) {
  if (m_handlerTable[path.string()] != nullptr) {
    m_handlerTable[path.string()](path);
  }
}

}  // namespace meerkat::filesystem
