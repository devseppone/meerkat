#include <meerkat/filesystem.hpp>
#include <meerkat/filesystem/event_loop.hpp>
#include <meerkat/filesystem/watcher.hpp>

#include <filesystem>
#include <fstream>
#include <functional>

namespace fs = std::filesystem;

namespace meerkat::filesystem {

void watch(fs::path const& path, FileEventHandler const& handler) {
  EventLoop::instance().add({path, handler});
}

}  // namespace meerkat::filesystem
